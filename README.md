# BHS18_Kinderzimmer_Belueftete_Sockelleiste

Arduino Sourcen und Schaltpläne für die Lüfteransteuerung einer belüfteten Sockelleiste.

# Hilfreiche Links
* [Ein ähnliches Projekt](https://www.elektormagazine.com/labs/holzfeuchtemessgerat-wood-moisture-content-meter)
* Eine Aussage zu kiritschen Werten für Holz- und Luftfeuchtigkeit findet sich [hier](https://www.woodproducts.fi/de/content/die-feuchtigkeitstechnischen-eigenschaften-von-holz) (Holz kritisch ab 20%, dabei ist meist die Luftfeuchte über 80%; Eine relative Luftfeuchtigkeit von 70% kann schon als kritischer Wert betrachtet werden.)
* Nützlicher [Artikel](http://www.vtt.fi/inf/pdf/publications/2000/P420.pdf) mit Widerstandswerte für verschiedene Holzsorten (ebenfalls verlinkt in obigem Projekt).
