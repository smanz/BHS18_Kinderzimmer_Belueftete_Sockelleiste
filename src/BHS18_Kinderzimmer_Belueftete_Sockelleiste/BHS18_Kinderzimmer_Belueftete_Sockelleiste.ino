 /**************************************************************************
The following libraries are used:

* Driver for I2C OLED display:
  SSD1306 by Adafruit (tested with version 2.3.1)
  https://github.com/adafruit/Adafruit_SSD1306

 **************************************************************************/

 #include <avr/sleep.h> 
#include <Wire.h>
#include <Adafruit_SSD1306.h>

// for watchdog wake up
volatile int sleepcounter = 0; // count sleep cycles

// Display
Adafruit_SSD1306 display(128, 64, &Wire, -1);  // width, height, , reset pin
bool display_enabled = false;
int mode;

// Pin definition
#define LED_RED_PIN 4
#define LED_YELLOW_PIN 5
#define LED_GREEN_PIN 6
#define MODE0_PIN 7
#define MODE1_PIN 8
#define MODE2_PIN 9
#define SAMPLE_H1 A0
#define SAMPLE_B1 A1
#define SAMPLE_H2 A2
#define SAMPLE_B2 A3

// thresholds for H1/2 / B1/2 for red/yellow/green led
#define THRESHOLD_RED_H1 512
#define THRESHOLD_RED_B1 512
#define THRESHOLD_RED_H2 512
#define THRESHOLD_RED_B2 512
#define THRESHOLD_YELLOW_H1 128
#define THRESHOLD_YELLOW_B1 128
#define THRESHOLD_YELLOW_H2 128
#define THRESHOLD_YELLOW_B2 128

// global variables holding the most recent measurements
int analog_samples[4];
float analog_samples_f[4];


void setup() {
    // ----------------Deep sleep
    prepareDeepSleep();
    //ADCSRA = ADCSRA & B01111111; // disable ADC (bit7)
    ACSR = B10000000; // Analog Comparator Disable (bit7)
    DIDR0 = DIDR0 | B00111111; // disable digital inputs on analog pins (bit 5-0)
    // ----------------
    
    Serial.begin(9600);

    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // I2C Address 0x3C
        Serial.println(F("Display not found, continue without display."));
        display_enabled = false;
    } else {
        Serial.println(F("Display found."));
        display_enabled = true;
    }
    // Clear the display buffer
    display.clearDisplay();
    
    pinMode(LED_RED_PIN, OUTPUT);
    pinMode(LED_YELLOW_PIN, OUTPUT);
    pinMode(LED_GREEN_PIN, OUTPUT);
    
    pinMode(MODE0_PIN, INPUT);
    pinMode(MODE1_PIN, INPUT);
    pinMode(MODE2_PIN, INPUT);
}

void loop() {
    // 0 wait for start of next cycle
    //TODO
    
    // 1.1 read the input pin
    analog_samples[0] = analogRead(SAMPLE_H1);
    analog_samples[1] = analogRead(SAMPLE_B1);
    analog_samples[2] = analogRead(SAMPLE_H2);
    analog_samples[3] = analogRead(SAMPLE_B2);

    analog_samples_f[0] = (float)analog_samples[0] * 3300.0 / 1024.0;
    analog_samples_f[1] = (float)analog_samples[1] * 3300.0 / 1024.0;
    analog_samples_f[2] = (float)analog_samples[2] * 3300.0 / 1024.0;
    analog_samples_f[3] = (float)analog_samples[3] * 3300.0 / 1024.0;

    Serial.print(F("H1: "));
    Serial.print(analog_samples_f[0]);
    Serial.print(F("mV "));
    Serial.print(analog_samples[0]);
    Serial.println(F(" raw"));
    Serial.print(F("B1: "));
    Serial.print(analog_samples_f[1]);
    Serial.print(F("mV "));
    Serial.print(analog_samples[1]);
    Serial.println(F(" raw"));

    // 1.2 read BME2
    //TODO
    
    // 2.1 update display and leds
    refresh_display();
    refresh_leds();

    delay(3000);  //TODO: remove
    
    //3.1 detect mode and continue accordingly
    mode = digitalRead(MODE0_PIN);
    mode |= (digitalRead(MODE1_PIN) << 1);
    mode |= (digitalRead(MODE2_PIN) << 2);
    
    mode &= 0x7;
    //TODO
   /* switch(mode)
    {
        case 0:
            
            break;
        case 1:
        default:
            break;
    }*/
    
    
    Serial.print(F("mode: "));
    Serial.print(mode);
    Serial.println(F(", deep sleep"));
    display.print(F("mode: "));
    display.print(mode);
    display.println(F(", deep sleep"));
    display.display();
    delay(100);
    enterDeepSleep(4);
}


void refresh_display(){
    display.clearDisplay();

    display.setTextSize(1);
    display.setTextColor(SSD1306_WHITE);
    display.setCursor(0,0);             // Start at top-left corner
    
    display.print(F("H1: "));
    display.print(analog_samples_f[0]);
    display.print(F("mV "));
    display.print(analog_samples[0]);
    display.println(F(" raw"));
    display.print(F("B1: "));
    display.print(analog_samples_f[1]);
    display.print(F("mV "));
    display.print(analog_samples[1]);
    display.println(F(" raw"));
    display.display();
}

void refresh_leds(){
    bool red_alert = false;
    bool yellow_alert = false;
   
    if (analog_samples[0] > THRESHOLD_RED_H1) {
        red_alert= true; 
    }else if (analog_samples[0] > THRESHOLD_YELLOW_H1) {
        yellow_alert = true;
    }
    
    if (analog_samples[1] > THRESHOLD_RED_B1) {
        red_alert= true;
    } else if (analog_samples[1] > THRESHOLD_YELLOW_B1) {
        yellow_alert = true;
    }
    
    if (red_alert) {
        digitalWrite(LED_RED_PIN, HIGH);
        digitalWrite(LED_YELLOW_PIN, LOW);
        digitalWrite(LED_GREEN_PIN, LOW);
    } else if (yellow_alert) {
        digitalWrite(LED_RED_PIN, LOW);
        digitalWrite(LED_YELLOW_PIN, HIGH);
        digitalWrite(LED_GREEN_PIN, LOW);
    } else {
        digitalWrite(LED_RED_PIN, LOW);
        digitalWrite(LED_YELLOW_PIN, LOW);
        digitalWrite(LED_GREEN_PIN, HIGH);
    }
}


// *****************************************Power save

void enterDeepSleep(int seconds_x8) {
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    for(int i=0; i < seconds_x8; i++) {
        sleep_enable();     // enable sleep mode
        sleep_mode();       // enter sleep mode
        sleep_disable();    // disable sleep mode after wake-up
    }
}

void prepareDeepSleep() {
    MCUSR = MCUSR & B11110111;    // disable Reset flag (bit3).
    WDTCSR = WDTCSR | B00011000;  // set bit 3 and 4 to make Prescaler writeable
    //  WDTCSR = B00000110;         // set Watchdog Prescaler to 128 cycles -> approx. 1s
    WDTCSR = B00100001;           // set Watchdog Prescaler to 1024k cycles -> approx. 8s
    WDTCSR = WDTCSR | B01000000;  // enable Watchdog Interrupt
    MCUSR = MCUSR & B11110111;
}

ISR(WDT_vect) {
    sleepcounter++;  // count sleep cycles
}
