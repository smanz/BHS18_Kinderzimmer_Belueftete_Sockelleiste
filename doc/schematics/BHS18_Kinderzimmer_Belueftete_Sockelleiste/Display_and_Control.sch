EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title "BHS18_Kinderzimmer_Belueftete_Sockelleiste"
Date "2020-09-21"
Rev "0.5"
Comp "smanz (privat)"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Display_Graphic:EA_DOGS104B-A U?
U 1 1 5F5F3349
P 7950 5150
AR Path="/5F5F3349" Ref="U?"  Part="1" 
AR Path="/5F5DAD9B/5F5F3349" Ref="U2"  Part="1" 
F 0 "U2" H 7950 5831 50  0000 C CNN
F 1 "EA_DOGS104B-A" H 7950 5740 50  0000 C CNN
F 2 "Display:EA_DOGS104X-A" H 7950 4550 50  0001 C CNN
F 3 "http://www.lcd-module.com/fileadmin/eng/pdf/doma/dogs104e.pdf" H 8350 4850 50  0001 C CNN
	1    7950 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 4400 7950 4650
Wire Wire Line
	7950 5650 7950 5850
NoConn ~ 8350 4850
NoConn ~ 8350 4950
NoConn ~ 7550 5450
NoConn ~ 7550 5250
NoConn ~ 7550 5350
Wire Wire Line
	7050 5050 7550 5050
Wire Wire Line
	7050 4950 7400 4950
Wire Wire Line
	7550 4850 7400 4850
Wire Wire Line
	7400 4850 7400 4950
Connection ~ 7400 4950
Wire Wire Line
	7400 4950 7550 4950
Text Notes 8350 5450 0    50   ~ 0
Das ist ein Platzhalter für\ndas 128x64 I2C OLED Display,\ndas ich verwende.
Text HLabel 1300 1750 0    50   BiDi ~ 0
SDA_OLED
Text HLabel 1300 1850 0    50   BiDi ~ 0
SCL_OLED
$Comp
L Device:LED D2
U 1 1 5F6524C7
P 5450 1450
F 0 "D2" H 5443 1667 50  0000 C CNN
F 1 "LED_red" H 5443 1576 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 5450 1450 50  0001 C CNN
F 3 "~" H 5450 1450 50  0001 C CNN
	1    5450 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5F653BDC
P 5450 1750
F 0 "D3" H 5443 1967 50  0000 C CNN
F 1 "LED_yellow" H 5443 1876 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 5450 1750 50  0001 C CNN
F 3 "~" H 5450 1750 50  0001 C CNN
	1    5450 1750
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D4
U 1 1 5F653F9C
P 5450 2050
F 0 "D4" H 5443 2267 50  0000 C CNN
F 1 "LED_green" H 5443 2176 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 5450 2050 50  0001 C CNN
F 3 "~" H 5450 2050 50  0001 C CNN
	1    5450 2050
	1    0    0    -1  
$EndComp
Text GLabel 7050 4950 0    50   BiDi ~ 0
SDA
Text GLabel 2850 1750 2    50   BiDi ~ 0
SDA
Text GLabel 7050 5050 0    50   BiDi ~ 0
SCL
Text GLabel 2850 1850 2    50   BiDi ~ 0
SCL
Text HLabel 1300 1550 0    50   Input ~ 0
3V3_Panel
Text HLabel 1300 1650 0    50   Input ~ 0
GND_Panel
$Comp
L power:+3.3V #PWR024
U 1 1 5F657172
P 3550 5200
F 0 "#PWR024" H 3550 5050 50  0001 C CNN
F 1 "+3.3V" H 3565 5373 50  0000 C CNN
F 2 "" H 3550 5200 50  0001 C CNN
F 3 "" H 3550 5200 50  0001 C CNN
	1    3550 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR023
U 1 1 5F658336
P 3350 5500
F 0 "#PWR023" H 3350 5250 50  0001 C CNN
F 1 "GND" H 3355 5327 50  0000 C CNN
F 2 "" H 3350 5500 50  0001 C CNN
F 3 "" H 3350 5500 50  0001 C CNN
	1    3350 5500
	1    0    0    -1  
$EndComp
Text GLabel 2850 1550 2    50   Output ~ 0
VCC_in
Text GLabel 2850 1650 2    50   Output ~ 0
Gnd_in
Text GLabel 3550 5400 3    50   Input ~ 0
VCC_in
Wire Wire Line
	3350 5300 3350 5500
Wire Wire Line
	3550 5200 3550 5400
Text HLabel 1300 1950 0    50   Input ~ 0
LED_Red_Panel
Text HLabel 1300 2050 0    50   Input ~ 0
LED_Yellow_Panel
Text HLabel 1300 2150 0    50   Input ~ 0
LED_Green_Panel
Text GLabel 2850 1950 2    50   Output ~ 0
LED_Red
Text GLabel 2850 2050 2    50   Output ~ 0
LED_Yellow
Text GLabel 2850 2150 2    50   Output ~ 0
LED_Green
Text GLabel 6250 1450 2    50   Input ~ 0
LED_Red
Text GLabel 6250 1750 2    50   Input ~ 0
LED_Yellow
Text GLabel 6250 2050 2    50   Input ~ 0
LED_Green
$Comp
L power:GND #PWR025
U 1 1 5F65D904
P 4700 3450
F 0 "#PWR025" H 4700 3200 50  0001 C CNN
F 1 "GND" H 4705 3277 50  0000 C CNN
F 2 "" H 4700 3450 50  0001 C CNN
F 3 "" H 4700 3450 50  0001 C CNN
	1    4700 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R9
U 1 1 5F65EF21
P 5900 1450
F 0 "R9" V 5704 1450 50  0000 C CNN
F 1 "1k" V 5795 1450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 5900 1450 50  0001 C CNN
F 3 "~" H 5900 1450 50  0001 C CNN
	1    5900 1450
	0    1    1    0   
$EndComp
Wire Wire Line
	6000 1450 6250 1450
Wire Wire Line
	6000 1750 6250 1750
Wire Wire Line
	6000 2050 6250 2050
Wire Wire Line
	5800 2050 5600 2050
Wire Wire Line
	5800 1750 5600 1750
Wire Wire Line
	5800 1450 5600 1450
Wire Wire Line
	5300 1750 4800 1750
Wire Wire Line
	4800 1750 4800 2050
Wire Wire Line
	5300 2050 4800 2050
Wire Notes Line
	6700 4000 9600 4000
Wire Notes Line
	9600 4000 9600 6400
Wire Notes Line
	9600 6400 6700 6400
Wire Notes Line
	6700 6400 6700 4000
Wire Notes Line
	3150 4800 3800 4800
Wire Notes Line
	3800 4800 3800 5800
Wire Notes Line
	3150 4800 3150 5800
Wire Notes Line
	3150 5800 3800 5800
Text Notes 1150 1050 0    50   ~ 0
Signals on Connector / Cabel
$Comp
L Device:R_Small R10
U 1 1 5F668826
P 5900 1750
F 0 "R10" V 5704 1750 50  0000 C CNN
F 1 "1k" V 5795 1750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 5900 1750 50  0001 C CNN
F 3 "~" H 5900 1750 50  0001 C CNN
	1    5900 1750
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R11
U 1 1 5F6689C3
P 5900 2050
F 0 "R11" V 5704 2050 50  0000 C CNN
F 1 "15k" V 5795 2050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 5900 2050 50  0001 C CNN
F 3 "~" H 5900 2050 50  0001 C CNN
	1    5900 2050
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_SPDT_MSM SW1
U 1 1 5F66D8DD
P 2650 6350
F 0 "SW1" H 2650 6635 50  0000 C CNN
F 1 "SW_SPDT_MSM" H 2650 6544 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 2650 6350 50  0001 C CNN
F 3 "~" H 2650 6350 50  0001 C CNN
	1    2650 6350
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW2
U 1 1 5F66E2A6
P 5450 4500
F 0 "SW2" H 5450 4735 50  0000 C CNN
F 1 "SW_SPST" H 5450 4644 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-0.1sqmm_1x02_P3.6mm_D0.4mm_OD1mm" H 5450 4500 50  0001 C CNN
F 3 "~" H 5450 4500 50  0001 C CNN
	1    5450 4500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW3
U 1 1 5F66E74A
P 5450 4900
F 0 "SW3" H 5450 5135 50  0000 C CNN
F 1 "SW_SPST" H 5450 5044 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-0.1sqmm_1x02_P3.6mm_D0.4mm_OD1mm" H 5450 4900 50  0001 C CNN
F 3 "~" H 5450 4900 50  0001 C CNN
	1    5450 4900
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW4
U 1 1 5F66EEA8
P 5450 5250
F 0 "SW4" H 5450 5485 50  0000 C CNN
F 1 "SW_SPST" H 5450 5394 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-0.1sqmm_1x02_P3.6mm_D0.4mm_OD1mm" H 5450 5250 50  0001 C CNN
F 3 "~" H 5450 5250 50  0001 C CNN
	1    5450 5250
	1    0    0    -1  
$EndComp
Text HLabel 1300 1250 0    50   Input ~ 0
5V_Panel
Text HLabel 1300 1350 0    50   Output ~ 0
5Vback_Fan
Text HLabel 1300 1450 0    50   Output ~ 0
5Vback_Electronic
Text GLabel 2850 1250 2    50   Output ~ 0
5Vin
Text GLabel 2850 1350 2    50   Input ~ 0
5Vback_Fan
Text GLabel 2850 1450 2    50   Input ~ 0
5Vback_Electronic
Text GLabel 2300 6350 0    50   Input ~ 0
5Vin
Text GLabel 3000 6450 2    50   Output ~ 0
5Vback_Fan
Text GLabel 3000 6250 2    50   Output ~ 0
5Vback_Electronic
Wire Wire Line
	3000 6250 2850 6250
Wire Wire Line
	3000 6450 2850 6450
Wire Wire Line
	2450 6350 2300 6350
Text Notes 2200 6800 0    50   ~ 0
Controls whether the fan is\non, off, or controlled by the \nmicrocontroller.
Wire Notes Line
	3800 6000 3800 6900
Wire Notes Line
	2000 6900 2000 6000
Connection ~ 4800 2050
Text Notes 4250 5600 0    50   ~ 0
TODO: \n* Sind die Widerstände gut gewählt?\n* Brauche ich hier Kondensatoren (besser am MCU)
$Comp
L Switch:SW_Push_Dual SW6
U 1 1 5F65DD62
P 4800 2700
F 0 "SW6" V 4754 2848 50  0000 L CNN
F 1 "SW_Push_Dual" V 4845 2848 50  0000 L CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H5mm" H 4800 2900 50  0001 C CNN
F 3 "~" H 4800 2900 50  0001 C CNN
	1    4800 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 1450 5300 1450
$Comp
L Device:Jumper JP2
U 1 1 5F665E31
P 4000 2800
F 0 "JP2" V 4046 2712 50  0000 R CNN
F 1 "Jumper" V 3955 2712 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4000 2800 50  0001 C CNN
F 3 "~" H 4000 2800 50  0001 C CNN
	1    4000 2800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4000 3100 4000 3150
Wire Wire Line
	4000 2300 4600 2300
Wire Wire Line
	4000 3150 4600 3150
Wire Wire Line
	4000 2300 4000 2500
Wire Notes Line
	6850 3750 3750 3750
Wire Notes Line
	3750 850  6850 850 
Text HLabel 1300 2250 0    50   Output ~ 0
Mode1_Panel
Text HLabel 1300 2350 0    50   Output ~ 0
Mode2_Panel
Text HLabel 1300 2450 0    50   Output ~ 0
Mode3_Panel
Text GLabel 2850 2250 2    50   Input ~ 0
M1
Text GLabel 2850 2350 2    50   Input ~ 0
M2
Text GLabel 2850 2450 2    50   Input ~ 0
M3
Text GLabel 6250 4300 1    50   Input ~ 0
VCC
Wire Wire Line
	5650 4500 6250 4500
Wire Wire Line
	6250 4500 6250 4300
Wire Wire Line
	5650 4900 6250 4900
Wire Wire Line
	6250 4900 6250 4500
Connection ~ 6250 4500
Wire Wire Line
	5650 5250 6250 5250
Wire Wire Line
	6250 5250 6250 4900
Connection ~ 6250 4900
Text GLabel 4700 4500 0    50   Output ~ 0
M1
Text GLabel 4700 4900 0    50   Output ~ 0
M2
Text GLabel 4700 5250 0    50   Output ~ 0
M3
$Comp
L Device:R_Small R5
U 1 1 5F67A912
P 5000 4500
F 0 "R5" V 4804 4500 50  0000 C CNN
F 1 "10k" V 4895 4500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 5000 4500 50  0001 C CNN
F 3 "~" H 5000 4500 50  0001 C CNN
	1    5000 4500
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5F67AE84
P 5000 4900
F 0 "R6" V 4804 4900 50  0000 C CNN
F 1 "10k" V 4895 4900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 5000 4900 50  0001 C CNN
F 3 "~" H 5000 4900 50  0001 C CNN
	1    5000 4900
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5F67B1A4
P 5000 5250
F 0 "R7" V 4804 5250 50  0000 C CNN
F 1 "10k" V 4895 5250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 5000 5250 50  0001 C CNN
F 3 "~" H 5000 5250 50  0001 C CNN
	1    5000 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 4500 5250 4500
Wire Wire Line
	5100 4900 5250 4900
Wire Wire Line
	5100 5250 5250 5250
Wire Wire Line
	4700 4500 4900 4500
Wire Wire Line
	4700 4900 4900 4900
Wire Wire Line
	4700 5250 4900 5250
Wire Notes Line
	6500 4000 6500 5800
Wire Notes Line
	6500 5800 4050 5800
Wire Notes Line
	4050 5800 4050 4000
Wire Notes Line
	4050 4000 6500 4000
$Comp
L Device:Jumper JP3
U 1 1 5F697232
P 5700 2750
F 0 "JP3" V 5654 2877 50  0000 L CNN
F 1 "Jumper" V 5745 2877 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5700 2750 50  0001 C CNN
F 3 "~" H 5700 2750 50  0001 C CNN
	1    5700 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	4600 1450 4600 2300
Connection ~ 4600 2300
Wire Wire Line
	4600 2300 4600 2500
Wire Wire Line
	4600 2900 4600 3150
Wire Wire Line
	4800 2900 4800 3150
Wire Wire Line
	4800 2050 4800 2300
Wire Wire Line
	5700 2450 5700 2300
Wire Wire Line
	5700 2300 4800 2300
Connection ~ 4800 2300
Wire Wire Line
	4800 2300 4800 2500
Wire Wire Line
	5700 3050 5700 3150
Wire Wire Line
	5700 3150 4800 3150
Wire Wire Line
	4700 3150 4700 3450
Wire Notes Line
	6850 850  6850 3750
Wire Notes Line
	3750 850  3750 3750
Wire Wire Line
	4800 3150 4700 3150
Connection ~ 4800 3150
Connection ~ 4600 3150
Connection ~ 4700 3150
Wire Wire Line
	4700 3150 4600 3150
$Comp
L Device:R_Small R8
U 1 1 5F6B081A
P 5200 6700
F 0 "R8" V 5004 6700 50  0000 C CNN
F 1 "10k" V 5095 6700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 5200 6700 50  0001 C CNN
F 3 "~" H 5200 6700 50  0001 C CNN
	1    5200 6700
	0    1    1    0   
$EndComp
Text GLabel 6200 6350 1    50   Input ~ 0
VCC
Text GLabel 4800 6700 0    50   Output ~ 0
Display_en
Wire Wire Line
	6200 6350 6200 6700
Wire Wire Line
	6200 6700 5950 6700
Wire Wire Line
	5550 6700 5300 6700
Wire Wire Line
	5100 6700 4800 6700
Text GLabel 2850 2550 2    50   Input ~ 0
Display_en
Text HLabel 1300 2550 0    50   Output ~ 0
Display_En_Panel
$Comp
L Switch:SW_SPST SW5
U 1 1 5F6B052B
P 5750 6700
F 0 "SW5" H 5750 6935 50  0000 C CNN
F 1 "SW_SPST" H 5750 6844 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-0.1sqmm_1x02_P3.6mm_D0.4mm_OD1mm" H 5750 6700 50  0001 C CNN
F 3 "~" H 5750 6700 50  0001 C CNN
	1    5750 6700
	1    0    0    -1  
$EndComp
Wire Notes Line
	4050 6000 6500 6000
Wire Notes Line
	6500 6000 6500 6900
Wire Notes Line
	6500 6900 4050 6900
Wire Notes Line
	4050 6900 4050 6000
Wire Notes Line
	2000 6000 3800 6000
Wire Notes Line
	2000 6900 3800 6900
Text Notes 4150 6150 0    50   ~ 0
Enables / Disables the display.
Text GLabel 3350 5300 1    50   Input ~ 0
Gnd_in
$Comp
L power:GND #PWR027
U 1 1 5F6AF852
P 7950 5850
F 0 "#PWR027" H 7950 5600 50  0001 C CNN
F 1 "GND" H 7955 5677 50  0000 C CNN
F 2 "" H 7950 5850 50  0001 C CNN
F 3 "" H 7950 5850 50  0001 C CNN
	1    7950 5850
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR026
U 1 1 5F6AFD96
P 7950 4400
F 0 "#PWR026" H 7950 4250 50  0001 C CNN
F 1 "+3.3V" H 7965 4573 50  0000 C CNN
F 2 "" H 7950 4400 50  0001 C CNN
F 3 "" H 7950 4400 50  0001 C CNN
	1    7950 4400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW8
U 1 1 5F91148E
P 7300 2000
F 0 "SW8" V 7254 2148 50  0000 L CNN
F 1 "SW_Push" V 7345 2148 50  0000 L CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm_H4.3mm" H 7300 2200 50  0001 C CNN
F 3 "~" H 7300 2200 50  0001 C CNN
	1    7300 2000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR036
U 1 1 5F9146A6
P 8300 3450
F 0 "#PWR036" H 8300 3200 50  0001 C CNN
F 1 "GND" H 8305 3277 50  0000 C CNN
F 2 "" H 8300 3450 50  0001 C CNN
F 3 "" H 8300 3450 50  0001 C CNN
	1    8300 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR034
U 1 1 5F914C39
P 7300 1300
F 0 "#PWR034" H 7300 1150 50  0001 C CNN
F 1 "+3.3V" H 7315 1473 50  0000 C CNN
F 2 "" H 7300 1300 50  0001 C CNN
F 3 "" H 7300 1300 50  0001 C CNN
	1    7300 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R22
U 1 1 5F91543F
P 7300 1550
F 0 "R22" V 7104 1550 50  0000 C CNN
F 1 "15k" V 7195 1550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 7300 1550 50  0001 C CNN
F 3 "~" H 7300 1550 50  0001 C CNN
	1    7300 1550
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_NPN_BCE Q1
U 1 1 5F917655
P 8200 1550
F 0 "Q1" H 8391 1596 50  0000 L CNN
F 1 "Q_NPN_BCE" H 8391 1505 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 8400 1650 50  0001 C CNN
F 3 "~" H 8200 1550 50  0001 C CNN
	1    8200 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BCE Q2
U 1 1 5F91848D
P 8200 2850
F 0 "Q2" H 8391 2896 50  0000 L CNN
F 1 "Q_NPN_BCE" H 8391 2805 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 8400 2950 50  0001 C CNN
F 3 "~" H 8200 2850 50  0001 C CNN
	1    8200 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR035
U 1 1 5F91AAD6
P 8300 2300
F 0 "#PWR035" H 8300 2050 50  0001 C CNN
F 1 "GND" H 8305 2127 50  0000 C CNN
F 2 "" H 8300 2300 50  0001 C CNN
F 3 "" H 8300 2300 50  0001 C CNN
	1    8300 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 3050 8300 3300
Wire Wire Line
	8300 2550 8700 2550
Wire Wire Line
	8300 2550 8300 2650
Wire Wire Line
	8300 1350 8300 1150
Wire Wire Line
	7300 2200 7300 2300
Wire Wire Line
	7300 1800 7300 1650
Wire Wire Line
	7300 1450 7300 1300
$Comp
L Device:LED D5
U 1 1 5F92CF25
P 9200 1150
F 0 "D5" H 9193 1367 50  0000 C CNN
F 1 "LED_red" H 9193 1276 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 9200 1150 50  0001 C CNN
F 3 "~" H 9200 1150 50  0001 C CNN
	1    9200 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R23
U 1 1 5F92D9DA
P 9600 1150
F 0 "R23" V 9404 1150 50  0000 C CNN
F 1 "1k" V 9495 1150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 9600 1150 50  0001 C CNN
F 3 "~" H 9600 1150 50  0001 C CNN
	1    9600 1150
	0    1    1    0   
$EndComp
Text GLabel 9900 1150 2    50   Input ~ 0
LED_Red
$Comp
L Device:LED D6
U 1 1 5F92E055
P 9200 2350
F 0 "D6" H 9193 2567 50  0000 C CNN
F 1 "LED_yellow" H 9193 2476 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 9200 2350 50  0001 C CNN
F 3 "~" H 9200 2350 50  0001 C CNN
	1    9200 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D7
U 1 1 5F92E9C0
P 9200 2650
F 0 "D7" H 9193 2867 50  0000 C CNN
F 1 "LED_green" H 9193 2776 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 9200 2650 50  0001 C CNN
F 3 "~" H 9200 2650 50  0001 C CNN
	1    9200 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R24
U 1 1 5F92F1B3
P 9600 2350
F 0 "R24" V 9404 2350 50  0000 C CNN
F 1 "1k" V 9495 2350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 9600 2350 50  0001 C CNN
F 3 "~" H 9600 2350 50  0001 C CNN
	1    9600 2350
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R25
U 1 1 5F92F6ED
P 9600 2650
F 0 "R25" V 9404 2650 50  0000 C CNN
F 1 "15k" V 9495 2650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 9600 2650 50  0001 C CNN
F 3 "~" H 9600 2650 50  0001 C CNN
	1    9600 2650
	0    1    1    0   
$EndComp
Text GLabel 9900 2350 2    50   Input ~ 0
LED_Yellow
Text GLabel 9900 2650 2    50   Input ~ 0
LED_Green
Wire Wire Line
	9050 2350 8800 2350
Wire Wire Line
	8800 2350 8800 2550
Wire Wire Line
	8800 2550 8800 2650
Wire Wire Line
	8800 2650 9050 2650
Connection ~ 8800 2550
Wire Wire Line
	9350 2650 9500 2650
Wire Wire Line
	9700 2650 9900 2650
Wire Wire Line
	9900 2350 9700 2350
Wire Wire Line
	9500 2350 9350 2350
Wire Wire Line
	8300 1150 8700 1150
Wire Wire Line
	9350 1150 9500 1150
Wire Wire Line
	9700 1150 9900 1150
Wire Wire Line
	7300 2300 8000 2300
Wire Wire Line
	8000 2300 8000 2850
Wire Notes Line
	7050 850  10500 850 
Wire Notes Line
	10500 850  10500 3750
Wire Notes Line
	10500 3750 7050 3750
Wire Notes Line
	7050 3750 7050 850 
Text Notes 7050 700  0    50   ~ 0
Alternativvorschlag, ohne double push button, dafür zwei transistoren...
$Comp
L Device:Jumper JP4
U 1 1 5F96CECD
P 8700 1600
F 0 "JP4" V 8654 1727 50  0000 L CNN
F 1 "Jumper" V 8745 1727 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8700 1600 50  0001 C CNN
F 3 "~" H 8700 1600 50  0001 C CNN
	1    8700 1600
	0    1    1    0   
$EndComp
Wire Wire Line
	8000 1550 8000 2300
Connection ~ 8000 2300
Wire Wire Line
	8300 1750 8300 1950
Wire Wire Line
	8300 1950 8700 1950
Wire Wire Line
	8700 1950 8700 1900
Connection ~ 8300 1950
Wire Wire Line
	8300 1950 8300 2300
Wire Wire Line
	8700 1300 8700 1150
Connection ~ 8700 1150
Wire Wire Line
	8700 1150 9050 1150
$Comp
L Device:Jumper JP5
U 1 1 5F984EAF
P 8700 2900
F 0 "JP5" V 8654 3027 50  0000 L CNN
F 1 "Jumper" V 8745 3027 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8700 2900 50  0001 C CNN
F 3 "~" H 8700 2900 50  0001 C CNN
	1    8700 2900
	0    1    1    0   
$EndComp
Wire Wire Line
	8700 2600 8700 2550
Connection ~ 8700 2550
Wire Wire Line
	8700 2550 8800 2550
Wire Wire Line
	8700 3200 8700 3300
Wire Wire Line
	8700 3300 8300 3300
Connection ~ 8300 3300
Wire Wire Line
	8300 3300 8300 3450
$Comp
L Connector_Generic:Conn_01x15 J?
U 1 1 6016D974
P 2000 3550
AR Path="/6016D974" Ref="J?"  Part="1" 
AR Path="/5F5DAD9B/6016D974" Ref="J16"  Part="1" 
F 0 "J16" V 2217 3546 50  0000 C CNN
F 1 "Conn_01x15" V 2126 3546 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x15_P2.54mm_Vertical" H 2000 3550 50  0001 C CNN
F 3 "~" H 2000 3550 50  0001 C CNN
	1    2000 3550
	0    1    1    0   
$EndComp
NoConn ~ 1300 3350
Connection ~ 1400 1450
Wire Wire Line
	1400 1450 2850 1450
Wire Wire Line
	1300 1450 1400 1450
Wire Wire Line
	1300 1850 1800 1850
Wire Wire Line
	1300 2050 2500 2050
Wire Wire Line
	1300 2150 2400 2150
Wire Wire Line
	2850 2550 2700 2550
Wire Wire Line
	2600 3350 2600 1950
Wire Wire Line
	2400 2150 2400 3350
Wire Wire Line
	2500 2050 2500 3350
Wire Wire Line
	1400 3350 1400 1450
Wire Wire Line
	1600 3350 1600 1350
Wire Wire Line
	1500 3350 1500 1250
Wire Wire Line
	1900 3350 1900 1650
Wire Wire Line
	2000 3350 2000 1550
Wire Wire Line
	1700 3350 1700 1750
Wire Wire Line
	1800 3350 1800 1850
Wire Wire Line
	2700 3350 2700 2550
Connection ~ 2700 2550
Wire Wire Line
	1300 2550 2700 2550
Wire Wire Line
	1300 2350 2200 2350
Wire Wire Line
	1300 2450 2100 2450
Wire Wire Line
	2200 2350 2200 3350
Wire Wire Line
	2300 2250 2300 3350
Wire Wire Line
	2100 3350 2100 2450
Connection ~ 2200 2350
Wire Wire Line
	2200 2350 2850 2350
Connection ~ 2400 2150
Wire Wire Line
	2400 2150 2850 2150
Connection ~ 2500 2050
Wire Wire Line
	2500 2050 2850 2050
Connection ~ 2600 1950
Wire Wire Line
	2600 1950 2850 1950
Wire Wire Line
	1300 1950 2600 1950
Connection ~ 2300 2250
Wire Wire Line
	2300 2250 2850 2250
Wire Wire Line
	1300 2250 2300 2250
Connection ~ 2100 2450
Wire Wire Line
	2100 2450 2850 2450
Connection ~ 2000 1550
Wire Wire Line
	2000 1550 2850 1550
Wire Wire Line
	1300 1550 2000 1550
Wire Wire Line
	1300 1650 1900 1650
Wire Wire Line
	1300 1750 1700 1750
Connection ~ 1900 1650
Wire Wire Line
	1900 1650 2850 1650
Connection ~ 1800 1850
Wire Wire Line
	1800 1850 2850 1850
Connection ~ 1700 1750
Wire Wire Line
	1700 1750 2850 1750
Wire Wire Line
	1300 1250 1500 1250
Connection ~ 1600 1350
Wire Wire Line
	1600 1350 2850 1350
Wire Wire Line
	1300 1350 1600 1350
Connection ~ 1500 1250
Wire Wire Line
	1500 1250 2850 1250
$EndSCHEMATC
