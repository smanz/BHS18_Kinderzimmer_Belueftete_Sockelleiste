EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "BHS18_Kinderzimmer_Belueftete_Sockelleiste"
Date "2020-09-21"
Rev "0.5"
Comp "smanz (privat)"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 3400 1800 0    50   Input ~ 0
GND
Text GLabel 3400 1500 0    50   Output ~ 0
TXO
Text GLabel 3400 1600 0    50   Input ~ 0
RXI
Text GLabel 3400 1900 0    50   Input ~ 0
2
Text GLabel 3400 2100 0    50   Output ~ 0
4
Text GLabel 3400 2200 0    50   Output ~ 0
5
Text GLabel 3400 2300 0    50   Output ~ 0
6
Text GLabel 3400 2400 0    50   Input ~ 0
7
Text GLabel 3400 2500 0    50   Input ~ 0
8
Text GLabel 3400 2600 0    50   Input ~ 0
9
$Comp
L Connector:Conn_01x12_Female J2
U 1 1 5F498187
P 3600 2000
F 0 "J2" H 3500 1300 50  0000 C CNN
F 1 "Conn_01x12_Female" V 3650 1800 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x12_P2.54mm_Vertical" H 3600 2000 50  0001 C CNN
F 3 "~" H 3600 2000 50  0001 C CNN
	1    3600 2000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x12_Female J6
U 1 1 5F49B7D6
P 4200 2000
F 0 "J6" H 4150 1300 50  0000 C CNN
F 1 "Conn_01x12_Female" V 4250 1800 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x12_P2.54mm_Vertical" H 4200 2000 50  0001 C CNN
F 3 "~" H 4200 2000 50  0001 C CNN
	1    4200 2000
	-1   0    0    -1  
$EndComp
Text GLabel 4400 1500 2    50   Input ~ 0
RAW
Text GLabel 4400 1600 2    50   Input ~ 0
GND
Text GLabel 5050 1800 2    50   Output ~ 0
VCC
Text GLabel 4400 1900 2    50   Input ~ 0
A3
Text GLabel 4400 2000 2    50   Input ~ 0
A2
Text GLabel 4400 2100 2    50   Input ~ 0
A1
Text GLabel 4400 2200 2    50   Input ~ 0
A0
Text GLabel 4400 2600 2    50   Output ~ 0
10
$Comp
L Connector:Conn_01x02_Female J5
U 1 1 5F4AAB52
P 4100 1850
F 0 "J5" H 3600 900 50  0000 L CNN
F 1 "Conn_01x02_Female" H 2950 800 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 4100 1850 50  0001 C CNN
F 3 "~" H 4100 1850 50  0001 C CNN
	1    4100 1850
	1    0    0    -1  
$EndComp
Text GLabel 3900 1950 0    50   BiDi ~ 0
A4
Text GLabel 3900 1850 0    50   BiDi ~ 0
A5
Text GLabel 3900 2800 3    50   Input ~ 0
GND
$Comp
L power:GND #PWR02
U 1 1 5F49C5C1
P 1600 1450
F 0 "#PWR02" H 1600 1200 50  0001 C CNN
F 1 "GND" H 1605 1277 50  0000 C CNN
F 2 "" H 1600 1450 50  0001 C CNN
F 3 "" H 1600 1450 50  0001 C CNN
	1    1600 1450
	-1   0    0    -1  
$EndComp
Text GLabel 2100 1400 3    50   Input ~ 0
VCC
$Comp
L power:+3V3 #PWR03
U 1 1 5F49D700
P 2100 1300
F 0 "#PWR03" H 2100 1150 50  0001 C CNN
F 1 "+3V3" H 2115 1473 50  0000 C CNN
F 2 "" H 2100 1300 50  0001 C CNN
F 3 "" H 2100 1300 50  0001 C CNN
	1    2100 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 1450 1600 1400
Wire Wire Line
	2100 1300 2100 1400
Text Notes 9000 8750 0    50   ~ 0
Es fehlt:\n* Temperatursensoren (an A6 und A7)
Text GLabel 1200 2450 0    50   BiDi ~ 0
A5
Text GLabel 1200 2550 0    50   BiDi ~ 0
A4
Wire Wire Line
	1200 2450 1550 2450
Wire Wire Line
	1200 2550 1450 2550
Wire Wire Line
	1550 2850 1750 2850
Connection ~ 1550 2450
Wire Wire Line
	1550 2450 1750 2450
Wire Wire Line
	1750 3150 1550 3150
Wire Wire Line
	1550 2450 1550 2850
Connection ~ 1550 2850
Wire Wire Line
	1550 2850 1550 3150
Wire Wire Line
	1450 2550 1450 2950
Wire Wire Line
	1450 3250 1750 3250
Connection ~ 1450 2550
Wire Wire Line
	1450 2550 1750 2550
Wire Wire Line
	1750 2950 1450 2950
Connection ~ 1450 2950
Wire Wire Line
	1450 2950 1450 3250
Text Notes 900  2350 0    50   ~ 0
Pullups sollten nicht nötig sein\n(todo: verify, muss auch ohne \nBMEs gehen, PU dürfen also \nnicht nur dort sein)
Wire Notes Line
	2400 3450 750  3450
Wire Notes Line
	750  3450 750  1950
Wire Notes Line
	750  1950 2400 1950
Wire Notes Line
	2400 1950 2400 3450
Wire Notes Line
	2400 800  2400 1900
Wire Notes Line
	2400 1900 750  1900
Wire Notes Line
	750  1900 750  800 
Wire Notes Line
	750  800  2400 800 
Wire Notes Line
	2850 3400 5400 3400
Wire Notes Line
	5400 3400 5400 900 
Wire Notes Line
	5400 900  2850 900 
Text GLabel 1750 2450 2    50   BiDi ~ 0
SCL_OLED
Text GLabel 1750 2550 2    50   BiDi ~ 0
SDA_OLED
Text GLabel 1750 2850 2    50   BiDi ~ 0
SCL_BME_1
Text GLabel 1750 2950 2    50   BiDi ~ 0
SDA_BME_1
Text GLabel 1750 3150 2    50   BiDi ~ 0
SCL_BME_2
Text GLabel 1750 3250 2    50   BiDi ~ 0
SDA_BME_2
Text Notes 3000 3350 0    50   ~ 0
Note:\n* A4 and A5 (for I2C) are off-grid.\n* Upper 6-pin connector (FTDI-Programmer) \n   and lower 3-pin connector are optional.
NoConn ~ 6650 2450
NoConn ~ 6650 2350
$Comp
L power:+5V #PWR011
U 1 1 5EC5D751
P 7050 1300
F 0 "#PWR011" H 7050 1150 50  0001 C CNN
F 1 "+5V" H 7065 1473 50  0000 C CNN
F 2 "" H 7050 1300 50  0001 C CNN
F 3 "" H 7050 1300 50  0001 C CNN
	1    7050 1300
	1    0    0    -1  
$EndComp
NoConn ~ 6650 2050
Text Notes 5800 2050 0    50   ~ 0
ENA not connected, rely\non internal pullup
$Comp
L power:GND #PWR015
U 1 1 5EC6DEC3
P 7800 1800
F 0 "#PWR015" H 7800 1550 50  0001 C CNN
F 1 "GND" H 7722 1763 50  0000 R CNN
F 2 "" H 7800 1800 50  0001 C CNN
F 3 "" H 7800 1800 50  0001 C CNN
	1    7800 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5EC6EA71
P 7800 1600
F 0 "C2" H 7918 1646 50  0000 L CNN
F 1 "2200u" H 7918 1555 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 7838 1450 50  0001 C CNN
F 3 "~" H 7800 1600 50  0001 C CNN
F 4 "C" H 7800 1600 50  0001 C CNN "Spice_Primitive"
F 5 "2200u" H 7800 1600 50  0001 C CNN "Spice_Model"
F 6 "Y" H 7800 1600 50  0001 C CNN "Spice_Netlist_Enabled"
	1    7800 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6650 2150 6450 2150
$Comp
L power:GND #PWR016
U 1 1 5EC61042
P 8250 3050
F 0 "#PWR016" H 8250 2800 50  0001 C CNN
F 1 "GND" H 8255 2877 50  0000 C CNN
F 2 "" H 8250 3050 50  0001 C CNN
F 3 "" H 8250 3050 50  0001 C CNN
	1    8250 3050
	1    0    0    -1  
$EndComp
NoConn ~ 7950 2650
$Comp
L Motor:Fan_3pin M1
U 1 1 5EC5FC1C
P 8250 2650
F 0 "M1" H 8408 2646 50  0000 L CNN
F 1 "Fan_3pin" H 8408 2555 50  0000 L CNN
F 2 "Connector:FanPinHeader_1x03_P2.54mm_Vertical" H 8250 2560 50  0001 C CNN
F 3 "http://www.hardwarecanucks.com/forum/attachments/new-builds/16287d1330775095-help-chassis-power-fan-connectors-motherboard-asus_p8z68.jpg" H 8250 2560 50  0001 C CNN
	1    8250 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5EC5C47A
P 7050 2800
F 0 "#PWR012" H 7050 2550 50  0001 C CNN
F 1 "GND" H 7055 2627 50  0000 C CNN
F 2 "" H 7050 2800 50  0001 C CNN
F 3 "" H 7050 2800 50  0001 C CNN
	1    7050 2800
	1    0    0    -1  
$EndComp
NoConn ~ 7450 2350
Text Notes 5750 3300 0    50   ~ 0
Ich habe den MCP14E6 verwendet,\nweil der bei mir rumlag ...
Wire Wire Line
	8250 2150 8250 2450
Wire Wire Line
	8250 2950 8250 3050
Wire Wire Line
	7050 1300 7050 1400
Text GLabel 6450 2150 0    50   Input ~ 0
10
Wire Notes Line
	5650 3400 8900 3400
Wire Notes Line
	8900 900  5650 900 
Wire Wire Line
	7050 1400 7800 1400
Connection ~ 7050 1400
Wire Wire Line
	7050 1400 7050 1850
Wire Wire Line
	7800 1750 7800 1800
Wire Wire Line
	7800 1450 7800 1400
Wire Notes Line
	8900 900  8900 3400
Wire Notes Line
	5650 900  5650 3400
Text Notes 9250 1350 0    50   ~ 0
Connector for optional  \nWemos D1 to send\nmeasurements to a server.
NoConn ~ 4400 2300
NoConn ~ 3400 2000
NoConn ~ 4000 2800
NoConn ~ 4100 2800
Text Label 4100 2800 3    50   ~ 0
A7
Text Label 4000 2800 3    50   ~ 0
A6
Text Label 3400 2000 2    50   ~ 0
3
Text GLabel 8600 1800 1    50   Input ~ 0
5V_Fan
Wire Wire Line
	8600 1800 8600 2150
Wire Wire Line
	8600 2150 8250 2150
$Comp
L Device:D D1
U 1 1 5F68FC08
P 7800 2150
F 0 "D1" H 7800 2275 50  0000 C CNN
F 1 "D" H 7800 2366 50  0000 C CNN
F 2 "Diode_THT:D_A-405_P2.54mm_Vertical_KathodeUp" H 7800 2150 50  0001 C CNN
F 3 "~" H 7800 2150 50  0001 C CNN
	1    7800 2150
	-1   0    0    1   
$EndComp
Wire Wire Line
	7050 2650 7050 2800
Wire Wire Line
	7450 2150 7650 2150
Wire Wire Line
	7950 2150 8250 2150
Connection ~ 8250 2150
Text Label 4400 2500 0    50   ~ 0
11
NoConn ~ 4400 2500
$Comp
L Connector:Conn_01x04_Male J7
U 1 1 5F6DAAA2
P 1200 3800
F 0 "J7" V 1035 3728 50  0000 C CNN
F 1 "Conn_01x04_Male" V 1126 3728 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1200 3800 50  0001 C CNN
F 3 "~" H 1200 3800 50  0001 C CNN
	1    1200 3800
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Male J8
U 1 1 5F6DC10A
P 2050 3800
F 0 "J8" V 1885 3728 50  0000 C CNN
F 1 "Conn_01x04_Male" V 1976 3728 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2050 3800 50  0001 C CNN
F 3 "~" H 2050 3800 50  0001 C CNN
	1    2050 3800
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 5F70E5B6
P 1600 1250
F 0 "#PWR01" H 1600 1100 50  0001 C CNN
F 1 "+5V" H 1615 1423 50  0000 C CNN
F 2 "" H 1600 1250 50  0001 C CNN
F 3 "" H 1600 1250 50  0001 C CNN
	1    1600 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 1300 1350 1300
Wire Wire Line
	1350 1150 1350 1300
Text GLabel 1100 4200 3    50   BiDi ~ 0
SCL_BME_1
Text GLabel 1000 4200 3    50   BiDi ~ 0
SDA_BME_1
Text GLabel 1950 4200 3    50   BiDi ~ 0
SCL_BME_2
Text GLabel 1850 4200 3    50   BiDi ~ 0
SDA_BME_2
$Comp
L power:+3V3 #PWR07
U 1 1 5F74E550
P 1450 4300
AR Path="/5F74E550" Ref="#PWR07"  Part="1" 
AR Path="/5F5F76B0/5F74E550" Ref="#PWR?"  Part="1" 
F 0 "#PWR07" H 1450 4150 50  0001 C CNN
F 1 "+3V3" H 1465 4473 50  0000 C CNN
F 2 "" H 1450 4300 50  0001 C CNN
F 3 "" H 1450 4300 50  0001 C CNN
	1    1450 4300
	-1   0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR09
U 1 1 5F74F90F
P 2300 4300
AR Path="/5F74F90F" Ref="#PWR09"  Part="1" 
AR Path="/5F5F76B0/5F74F90F" Ref="#PWR?"  Part="1" 
F 0 "#PWR09" H 2300 4150 50  0001 C CNN
F 1 "+3V3" H 2315 4473 50  0000 C CNN
F 2 "" H 2300 4300 50  0001 C CNN
F 3 "" H 2300 4300 50  0001 C CNN
	1    2300 4300
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5F7504E9
P 2300 4500
AR Path="/5F7504E9" Ref="#PWR010"  Part="1" 
AR Path="/5F5F76B0/5F7504E9" Ref="#PWR?"  Part="1" 
F 0 "#PWR010" H 2300 4250 50  0001 C CNN
F 1 "GND" H 2305 4327 50  0000 C CNN
F 2 "" H 2300 4500 50  0001 C CNN
F 3 "" H 2300 4500 50  0001 C CNN
	1    2300 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 4300 2300 4350
Wire Wire Line
	2300 4350 2150 4350
Wire Wire Line
	2150 4350 2150 4000
Wire Wire Line
	1450 4300 1450 4350
Wire Wire Line
	1450 4350 1300 4350
Wire Wire Line
	1300 4350 1300 4000
Wire Wire Line
	1000 4000 1000 4200
Wire Wire Line
	1100 4000 1100 4200
Wire Wire Line
	1850 4000 1850 4200
Wire Wire Line
	1950 4000 1950 4200
$Comp
L power:GND #PWR08
U 1 1 5F74FD3C
P 1450 4500
AR Path="/5F74FD3C" Ref="#PWR08"  Part="1" 
AR Path="/5F5F76B0/5F74FD3C" Ref="#PWR?"  Part="1" 
F 0 "#PWR08" H 1450 4250 50  0001 C CNN
F 1 "GND" H 1455 4327 50  0000 C CNN
F 2 "" H 1450 4500 50  0001 C CNN
F 3 "" H 1450 4500 50  0001 C CNN
	1    1450 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 4450 1200 4450
Wire Wire Line
	1200 4450 1200 4000
Wire Wire Line
	1450 4450 1450 4500
Wire Wire Line
	2050 4450 2300 4450
Wire Wire Line
	2300 4450 2300 4500
Wire Wire Line
	2050 4000 2050 4450
Wire Notes Line
	2400 3550 2400 4750
Wire Notes Line
	2400 4750 750  4750
Wire Notes Line
	750  4750 750  3550
Wire Notes Line
	750  3550 2400 3550
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5F6B29B8
P 4950 1800
F 0 "#FLG01" H 4950 1875 50  0001 C CNN
F 1 "PWR_FLAG" H 4950 1973 50  0000 C CNN
F 2 "" H 4950 1800 50  0001 C CNN
F 3 "~" H 4950 1800 50  0001 C CNN
	1    4950 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 1800 4950 1800
Connection ~ 4950 1800
Wire Wire Line
	4400 1800 4950 1800
$Comp
L Connector:Conn_01x08_Female J12
U 1 1 5F6D31A5
P 9950 2150
F 0 "J12" H 9900 1650 50  0000 L CNN
F 1 "Conn_01x08_Female" V 10000 1750 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical" H 9950 2150 50  0001 C CNN
F 3 "~" H 9950 2150 50  0001 C CNN
	1    9950 2150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female J13
U 1 1 5F6D4CB7
P 10250 2150
F 0 "J13" H 10250 1650 50  0000 C CNN
F 1 "Conn_01x08_Female" V 10300 2150 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x08_P2.54mm_Vertical" H 10250 2150 50  0001 C CNN
F 3 "~" H 10250 2150 50  0001 C CNN
	1    10250 2150
	-1   0    0    -1  
$EndComp
Text GLabel 10800 1850 2    50   Input ~ 0
VCC
Text GLabel 9500 1950 0    50   Input ~ 0
GND
Text GLabel 10450 2850 0    50   Input ~ 0
12
Text GLabel 9600 2950 2    50   Input ~ 0
TXO
Text GLabel 9600 2850 2    50   Output ~ 0
RXI
Wire Wire Line
	10800 1850 10450 1850
Wire Wire Line
	10800 2550 10450 2550
Wire Wire Line
	9750 1950 9500 1950
Text Notes 10500 2550 0    50   ~ 0
Rst
Text Notes 10500 1850 0    50   ~ 0
3V3
Text Notes 9550 1950 0    50   ~ 0
GND
Text Notes 9650 2450 0    50   ~ 0
RX
Wire Notes Line
	9050 900  9050 3400
Wire Notes Line
	9050 3400 11050 3400
Wire Notes Line
	11050 3400 11050 900 
Wire Notes Line
	11050 900  9050 900 
NoConn ~ 9750 1850
NoConn ~ 9750 2050
NoConn ~ 9750 2150
NoConn ~ 9750 2250
NoConn ~ 9750 2350
NoConn ~ 10450 2450
NoConn ~ 10450 2350
NoConn ~ 10450 2250
NoConn ~ 10450 2150
NoConn ~ 10450 2050
NoConn ~ 10450 1950
Text Notes 4400 2300 0    50   ~ 0
13
Text GLabel 4400 2400 2    50   Output ~ 0
12
Text Notes 4600 2300 0    50   ~ 0
on-board led
$Comp
L Device:R_Small R?
U 1 1 5F9FAB94
P 9300 2700
AR Path="/5F5DAD9B/5F9FAB94" Ref="R?"  Part="1" 
AR Path="/5F9FAB94" Ref="R19"  Part="1" 
F 0 "R19" H 9500 2650 50  0000 R CNN
F 1 "220" H 9500 2750 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 9300 2700 50  0001 C CNN
F 3 "~" H 9300 2700 50  0001 C CNN
	1    9300 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	9300 2450 9300 2600
Wire Wire Line
	9300 2450 9750 2450
Wire Wire Line
	9600 2950 9300 2950
Wire Wire Line
	9300 2950 9300 2800
Wire Wire Line
	9600 2850 9400 2850
Wire Wire Line
	9400 2850 9400 2800
Text Notes 9650 2550 0    50   ~ 0
TX
$Comp
L Device:R_Small R?
U 1 1 5F9F9938
P 9400 2700
AR Path="/5F5DAD9B/5F9F9938" Ref="R?"  Part="1" 
AR Path="/5F9F9938" Ref="R20"  Part="1" 
F 0 "R20" H 9341 2654 50  0000 R CNN
F 1 "220" H 9341 2745 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 9400 2700 50  0001 C CNN
F 3 "~" H 9400 2700 50  0001 C CNN
	1    9400 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	9750 2550 9400 2550
Wire Wire Line
	9400 2550 9400 2600
$Comp
L Device:R_Small R?
U 1 1 5FA9B7EB
P 10800 2700
AR Path="/5F5DAD9B/5FA9B7EB" Ref="R?"  Part="1" 
AR Path="/5FA9B7EB" Ref="R21"  Part="1" 
F 0 "R21" H 10741 2654 50  0000 R CNN
F 1 "220" H 10741 2745 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 10800 2700 50  0001 C CNN
F 3 "~" H 10800 2700 50  0001 C CNN
	1    10800 2700
	-1   0    0    1   
$EndComp
Wire Wire Line
	10450 2850 10800 2850
Wire Wire Line
	10800 2850 10800 2800
Wire Wire Line
	10800 2600 10800 2550
Wire Notes Line
	3750 2750 4050 1900
Wire Notes Line
	3750 2750 3750 2950
Wire Notes Line
	3750 2950 2900 2950
Wire Notes Line
	3750 2750 2900 2750
Wire Notes Line
	2850 900  2850 3400
Wire Notes Line
	2900 2750 2900 2950
$Comp
L power:GND #PWR033
U 1 1 5F88D717
P 2250 1500
F 0 "#PWR033" H 2250 1250 50  0001 C CNN
F 1 "GND" H 2255 1327 50  0000 C CNN
F 2 "" H 2250 1500 50  0001 C CNN
F 3 "" H 2250 1500 50  0001 C CNN
	1    2250 1500
	-1   0    0    -1  
$EndComp
Text GLabel 2250 1400 1    50   Output ~ 0
GND
Wire Wire Line
	2250 1400 2250 1500
Wire Wire Line
	1600 1250 1600 1300
Text Notes 3050 1000 0    50   ~ 0
Connector for Arduino Pro Mini
Text Notes 4150 1250 1    50   ~ 0
GND
Text Notes 4050 1250 1    50   ~ 0
GND
Text Notes 3950 1250 1    50   ~ 0
VCC
Text Notes 3850 1250 1    50   ~ 0
RXI
Text Notes 3650 1250 1    50   ~ 0
DTR
Text Notes 3750 1250 1    50   ~ 0
TXO
NoConn ~ 4150 1250
NoConn ~ 4050 1250
NoConn ~ 3950 1250
NoConn ~ 3850 1250
NoConn ~ 3750 1250
NoConn ~ 3650 1250
$Comp
L Connector:Conn_01x06_Female J3
U 1 1 5F4AC71F
P 3950 1450
F 0 "J3" V 3650 1700 50  0000 L CNN
F 1 "Conn_01x06_Female" V 3750 1700 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 3950 1450 50  0001 C CNN
F 3 "~" H 3950 1450 50  0001 C CNN
	1    3950 1450
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Female J4
U 1 1 5F4A6229
P 4000 2600
F 0 "J4" V 3750 2450 50  0000 R CNN
F 1 "Conn_01x03_Female" V 3650 2450 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 4000 2600 50  0001 C CNN
F 3 "~" H 4000 2600 50  0001 C CNN
	1    4000 2600
	0    -1   -1   0   
$EndComp
$Comp
L Driver_FET:MCP14A0303xMNY U1
U 1 1 5EC5A9B1
P 7050 2250
F 0 "U1" H 7050 2831 50  0000 C CNN
F 1 "MCP14A0303xMNY" H 7050 2740 50  0000 C CNN
F 2 "Package_DFN_QFN:WDFN-8-1EP_3x2mm_P0.5mm_EP1.3x1.4mm" H 7050 3000 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/MCP14A0303_4_5-Data-Sheet-20006046A.pdf" H 7050 1950 50  0001 C CNN
	1    7050 2250
	1    0    0    -1  
$EndComp
Connection ~ 5300 4950
Wire Wire Line
	5400 4950 5300 4950
Wire Wire Line
	5300 4950 5200 4950
$Comp
L power:+3V3 #PWR028
U 1 1 5F710898
P 5300 4900
AR Path="/5F710898" Ref="#PWR028"  Part="1" 
AR Path="/5F5F76B0/5F710898" Ref="#PWR?"  Part="1" 
F 0 "#PWR028" H 5300 4750 50  0001 C CNN
F 1 "+3V3" H 5315 5073 50  0000 C CNN
F 2 "" H 5300 4900 50  0001 C CNN
F 3 "" H 5300 4900 50  0001 C CNN
	1    5300 4900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5200 5000 5200 4950
Wire Wire Line
	5300 5000 5300 4950
Wire Wire Line
	5400 5000 5400 4950
Wire Wire Line
	5300 4900 5300 4950
$Comp
L Device:R_Small R?
U 1 1 5F6BC06B
P 5200 5750
AR Path="/5F5DAD9B/5F6BC06B" Ref="R?"  Part="1" 
AR Path="/5F6BC06B" Ref="R12"  Part="1" 
F 0 "R12" H 4700 5700 50  0000 L CNN
F 1 "100k" H 4700 5800 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 5200 5750 50  0001 C CNN
F 3 "~" H 5200 5750 50  0001 C CNN
	1    5200 5750
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F6BC071
P 5300 5750
AR Path="/5F5DAD9B/5F6BC071" Ref="R?"  Part="1" 
AR Path="/5F6BC071" Ref="R13"  Part="1" 
F 0 "R13" H 4850 5700 50  0000 R CNN
F 1 "100k" H 4850 5800 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 5300 5750 50  0001 C CNN
F 3 "~" H 5300 5750 50  0001 C CNN
	1    5300 5750
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5F6BC077
P 5400 5750
AR Path="/5F5DAD9B/5F6BC077" Ref="R?"  Part="1" 
AR Path="/5F6BC077" Ref="R14"  Part="1" 
F 0 "R14" H 4850 5700 50  0000 R CNN
F 1 "100k" H 4800 5800 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 5400 5750 50  0001 C CNN
F 3 "~" H 5400 5750 50  0001 C CNN
	1    5400 5750
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_DIP_x03 SW7
U 1 1 5F6C5A0B
P 5200 5300
F 0 "SW7" H 5200 5767 50  0000 C CNN
F 1 "SW_DIP_x03" H 5200 5676 50  0000 C CNN
F 2 "Button_Switch_THT:SW_DIP_SPSTx03_Piano_CTS_Series194-3MSTN_W7.62mm_P2.54mm" H 5200 5300 50  0001 C CNN
F 3 "~" H 5200 5300 50  0001 C CNN
	1    5200 5300
	0    1    1    0   
$EndComp
Wire Wire Line
	5200 5600 5200 5650
Wire Wire Line
	5300 5600 5300 5650
Wire Wire Line
	5400 5600 5400 5650
Wire Wire Line
	5300 5850 5300 6050
Wire Wire Line
	5400 5850 5400 5950
Wire Wire Line
	5200 5850 5200 6150
Wire Wire Line
	5400 5950 5400 6550
Wire Wire Line
	5300 6050 5300 6600
Wire Wire Line
	5200 6150 5200 6650
Wire Wire Line
	5400 5950 5800 5950
Connection ~ 5400 5950
Wire Wire Line
	5300 6050 5800 6050
Connection ~ 5300 6050
Wire Wire Line
	5200 6150 5800 6150
Connection ~ 5200 6150
Wire Wire Line
	5200 6650 5750 6650
Wire Wire Line
	5750 6650 5750 6750
Connection ~ 5750 6750
Wire Wire Line
	4500 7300 4500 7400
Connection ~ 6700 6750
Wire Wire Line
	5600 6300 5800 6300
Connection ~ 5600 6300
Wire Wire Line
	6700 6450 6700 6750
Wire Wire Line
	5600 6450 6700 6450
Wire Wire Line
	5600 6300 5600 6450
Wire Wire Line
	5400 6550 6350 6550
Connection ~ 6050 6750
Wire Wire Line
	6050 6600 6050 6750
Wire Wire Line
	5300 6600 6050 6600
Wire Wire Line
	6050 7050 6050 7100
Connection ~ 6050 7050
Wire Wire Line
	6200 7050 6050 7050
Wire Wire Line
	6200 7000 6200 7050
Wire Wire Line
	6200 6750 6200 6800
Wire Wire Line
	6050 6750 6200 6750
Wire Wire Line
	6050 6800 6050 6750
$Comp
L Device:C_Small C7
U 1 1 5F758AE9
P 6200 6900
AR Path="/5F758AE9" Ref="C7"  Part="1" 
AR Path="/5F5F76B0/5F758AE9" Ref="C?"  Part="1" 
F 0 "C7" H 6150 7400 50  0000 L CNN
F 1 "100n" V 6200 7450 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D4.5mm_P2.50mm" H 6200 6900 50  0001 C CNN
F 3 "~" H 6200 6900 50  0001 C CNN
	1    6200 6900
	1    0    0    1   
$EndComp
Wire Wire Line
	6050 7000 6050 7050
$Comp
L Device:R_Small R?
U 1 1 5F758AE2
P 6050 6900
AR Path="/5F5DAD9B/5F758AE2" Ref="R?"  Part="1" 
AR Path="/5F758AE2" Ref="R16"  Part="1" 
F 0 "R16" H 6050 7400 50  0000 C CNN
F 1 "500k" V 6050 7550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 6050 6900 50  0001 C CNN
F 3 "~" H 6050 6900 50  0001 C CNN
	1    6050 6900
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR030
U 1 1 5F758ADC
P 6050 7100
F 0 "#PWR030" H 6050 6850 50  0001 C CNN
F 1 "GND" H 6055 6927 50  0000 C CNN
F 2 "" H 6050 7100 50  0001 C CNN
F 3 "" H 6050 7100 50  0001 C CNN
	1    6050 7100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5750 7050 5750 7100
Connection ~ 5750 7050
Wire Wire Line
	5900 7050 5750 7050
Wire Wire Line
	5900 7000 5900 7050
Wire Wire Line
	5900 6750 5900 6800
Wire Wire Line
	5750 6750 5900 6750
Wire Wire Line
	5750 6800 5750 6750
Wire Wire Line
	5750 7000 5750 7050
$Comp
L Device:R_Small R?
U 1 1 5F750F64
P 5750 6900
AR Path="/5F5DAD9B/5F750F64" Ref="R?"  Part="1" 
AR Path="/5F750F64" Ref="R15"  Part="1" 
F 0 "R15" H 5750 7400 50  0000 C CNN
F 1 "500k" V 5750 7550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 5750 6900 50  0001 C CNN
F 3 "~" H 5750 6900 50  0001 C CNN
	1    5750 6900
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR029
U 1 1 5F750F5E
P 5750 7100
F 0 "#PWR029" H 5750 6850 50  0001 C CNN
F 1 "GND" H 5755 6927 50  0000 C CNN
F 2 "" H 5750 7100 50  0001 C CNN
F 3 "" H 5750 7100 50  0001 C CNN
	1    5750 7100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6700 7050 6700 7100
Connection ~ 6700 7050
Wire Wire Line
	6850 7050 6700 7050
Wire Wire Line
	6850 7000 6850 7050
Wire Wire Line
	6850 6750 6850 6800
Wire Wire Line
	6700 6750 6850 6750
Wire Wire Line
	6700 6800 6700 6750
$Comp
L Device:C_Small C9
U 1 1 5F74905F
P 6850 6900
AR Path="/5F74905F" Ref="C9"  Part="1" 
AR Path="/5F5F76B0/5F74905F" Ref="C?"  Part="1" 
F 0 "C9" H 6800 7400 50  0000 L CNN
F 1 "100n" V 6850 7450 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D4.5mm_P2.50mm" H 6850 6900 50  0001 C CNN
F 3 "~" H 6850 6900 50  0001 C CNN
	1    6850 6900
	1    0    0    1   
$EndComp
Wire Wire Line
	6700 7000 6700 7050
$Comp
L Device:R_Small R?
U 1 1 5F749058
P 6700 6900
AR Path="/5F5DAD9B/5F749058" Ref="R?"  Part="1" 
AR Path="/5F749058" Ref="R18"  Part="1" 
F 0 "R18" H 6700 7400 50  0000 C CNN
F 1 "500k" V 6700 7550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 6700 6900 50  0001 C CNN
F 3 "~" H 6700 6900 50  0001 C CNN
	1    6700 6900
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR032
U 1 1 5F749052
P 6700 7100
F 0 "#PWR032" H 6700 6850 50  0001 C CNN
F 1 "GND" H 6705 6927 50  0000 C CNN
F 2 "" H 6700 7100 50  0001 C CNN
F 3 "" H 6700 7100 50  0001 C CNN
	1    6700 7100
	-1   0    0    -1  
$EndComp
Connection ~ 6350 6750
Wire Wire Line
	6350 6750 6350 6550
Wire Wire Line
	6350 7050 6350 7100
Connection ~ 6350 7050
Wire Wire Line
	6500 7050 6350 7050
Wire Wire Line
	6500 7000 6500 7050
Wire Wire Line
	6500 6750 6500 6800
Wire Wire Line
	6350 6750 6500 6750
Wire Wire Line
	6350 6800 6350 6750
$Comp
L Device:C_Small C8
U 1 1 5F71E5A8
P 6500 6900
AR Path="/5F71E5A8" Ref="C8"  Part="1" 
AR Path="/5F5F76B0/5F71E5A8" Ref="C?"  Part="1" 
F 0 "C8" H 6450 7400 50  0000 L CNN
F 1 "100n" V 6500 7450 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D4.5mm_P2.50mm" H 6500 6900 50  0001 C CNN
F 3 "~" H 6500 6900 50  0001 C CNN
	1    6500 6900
	1    0    0    1   
$EndComp
Wire Wire Line
	6350 7000 6350 7050
$Comp
L Device:R_Small R?
U 1 1 5F6F2A9A
P 6350 6900
AR Path="/5F5DAD9B/5F6F2A9A" Ref="R?"  Part="1" 
AR Path="/5F6F2A9A" Ref="R17"  Part="1" 
F 0 "R17" H 6350 7400 50  0000 C CNN
F 1 "500k" V 6350 7550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 6350 6900 50  0001 C CNN
F 3 "~" H 6350 6900 50  0001 C CNN
	1    6350 6900
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR031
U 1 1 5F6EB621
P 6350 7100
F 0 "#PWR031" H 6350 6850 50  0001 C CNN
F 1 "GND" H 6355 6927 50  0000 C CNN
F 2 "" H 6350 7100 50  0001 C CNN
F 3 "" H 6350 7100 50  0001 C CNN
	1    6350 7100
	-1   0    0    -1  
$EndComp
Text GLabel 5800 5950 2    50   Output ~ 0
7
Text GLabel 5800 6050 2    50   Output ~ 0
8
Text GLabel 5800 6150 2    50   Output ~ 0
9
Wire Wire Line
	4250 7500 5200 7500
Wire Wire Line
	4500 7400 5200 7400
Wire Wire Line
	4750 7600 5200 7600
Connection ~ 4750 7600
Connection ~ 4500 7400
Connection ~ 4250 7500
$Comp
L Jumper:Jumper_3_Open JP1
U 1 1 5F6D3909
P 4500 7150
F 0 "JP1" H 4500 7374 50  0000 C CNN
F 1 "Jumper_3_Open" H 4500 7283 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4500 7150 50  0001 C CNN
F 3 "~" H 4500 7150 50  0001 C CNN
	1    4500 7150
	-1   0    0    -1  
$EndComp
Text GLabel 4150 5800 2    50   Input ~ 0
6
Text GLabel 4150 5700 2    50   Input ~ 0
5
Text GLabel 4150 5600 2    50   Input ~ 0
4
Text Notes 5950 6350 0    50   ~ 0
Pin with Interrupt -> Pin 2 or 3
Text GLabel 5800 6300 2    50   Output ~ 0
2
Text GLabel 4150 6600 2    50   BiDi ~ 0
SDA_OLED
Text GLabel 4150 6500 2    50   BiDi ~ 0
SCL_OLED
Wire Wire Line
	4900 6700 4900 6750
Wire Wire Line
	4900 6850 4900 6900
$Comp
L power:+3V3 #PWR04
U 1 1 5F665FF7
P 4900 6700
F 0 "#PWR04" H 4900 6550 50  0001 C CNN
F 1 "+3V3" H 4915 6873 50  0000 C CNN
F 2 "" H 4900 6700 50  0001 C CNN
F 3 "" H 4900 6700 50  0001 C CNN
	1    4900 6700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5F66572D
P 4900 6900
F 0 "#PWR05" H 4900 6650 50  0001 C CNN
F 1 "GND" H 4800 6800 50  0000 C CNN
F 2 "" H 4900 6900 50  0001 C CNN
F 3 "" H 4900 6900 50  0001 C CNN
	1    4900 6900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4100 7400 4500 7400
Wire Wire Line
	4100 7000 4100 7400
Wire Wire Line
	4000 7500 4250 7500
Wire Wire Line
	4000 7100 4000 7500
Wire Wire Line
	3900 7600 4750 7600
Wire Wire Line
	3900 7200 3900 7600
$Sheet
S 750  5450 1650 1900
U 5F5DAD9B
F0 "Display_and_Control" 50
F1 "Display_and_Control.sch" 50
F2 "SDA_OLED" B R 2400 6600 50 
F3 "SCL_OLED" B R 2400 6500 50 
F4 "3V3_Panel" I R 2400 6750 50 
F5 "GND_Panel" I R 2400 6850 50 
F6 "LED_Red_Panel" I R 2400 5600 50 
F7 "LED_Yellow_Panel" I R 2400 5700 50 
F8 "LED_Green_Panel" I R 2400 5800 50 
F9 "5V_Panel" I R 2400 7000 50 
F10 "5Vback_Fan" O R 2400 7100 50 
F11 "5Vback_Electronic" O R 2400 7200 50 
F12 "Mode1_Panel" O R 2400 5950 50 
F13 "Mode2_Panel" O R 2400 6050 50 
F14 "Mode3_Panel" O R 2400 6150 50 
F15 "Display_En_Panel" O R 2400 6300 50 
$EndSheet
Text GLabel 5200 7500 2    50   Output ~ 0
5V_Fan
Wire Wire Line
	4250 7500 4250 7150
Wire Wire Line
	4750 7150 4750 7600
Wire Wire Line
	5200 7400 5200 7300
Text GLabel 5200 7600 2    50   Output ~ 0
RAW
$Comp
L power:+5V #PWR06
U 1 1 5F613106
P 5200 7300
F 0 "#PWR06" H 5200 7150 50  0001 C CNN
F 1 "+5V" H 5215 7473 50  0000 C CNN
F 2 "" H 5200 7300 50  0001 C CNN
F 3 "" H 5200 7300 50  0001 C CNN
	1    5200 7300
	1    0    0    -1  
$EndComp
Wire Notes Line
	11050 3550 6600 3550
Wire Notes Line
	11050 5600 11050 3550
Wire Notes Line
	6600 5600 11050 5600
Wire Notes Line
	6600 3550 6600 5600
Wire Wire Line
	10450 4250 10450 4550
$Comp
L Connector:Conn_01x02_Male J14
U 1 1 5F859A52
P 10250 4150
F 0 "J14" H 10223 4124 50  0000 R CNN
F 1 "Messpunkt4" H 10223 4033 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 10250 4150 50  0001 C CNN
F 3 "~" H 10250 4150 50  0001 C CNN
	1    10250 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10450 4550 10100 4550
Text GLabel 10100 4550 0    50   Output ~ 0
A3
Wire Wire Line
	10450 4550 10450 4650
Connection ~ 10450 4550
Wire Wire Line
	10650 4550 10450 4550
Wire Wire Line
	10650 4700 10650 4550
Connection ~ 10450 5050
Wire Wire Line
	10450 5050 10450 5150
Wire Wire Line
	10450 5050 10650 5050
Wire Wire Line
	10650 4900 10650 5050
Wire Wire Line
	10450 4950 10450 5050
Wire Wire Line
	10450 4050 10450 4150
$Comp
L Device:R R4
U 1 1 5F859A3C
P 10450 4800
AR Path="/5F859A3C" Ref="R4"  Part="1" 
AR Path="/5F5F76B0/5F859A3C" Ref="R?"  Part="1" 
F 0 "R4" H 10519 4754 50  0000 L CNN
F 1 "5M" H 10519 4845 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" V 10380 4800 50  0001 C CNN
F 3 "~" H 10450 4800 50  0001 C CNN
F 4 "R" H 10450 4800 50  0001 C CNN "Spice_Primitive"
F 5 "1Meg" H 10450 4800 50  0001 C CNN "Spice_Model"
F 6 "Y" H 10450 4800 50  0001 C CNN "Spice_Netlist_Enabled"
	1    10450 4800
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5F859A2F
P 10650 4800
AR Path="/5F859A2F" Ref="C5"  Part="1" 
AR Path="/5F5F76B0/5F859A2F" Ref="C?"  Part="1" 
F 0 "C5" H 10742 4754 50  0000 L CNN
F 1 "100n" H 10742 4845 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D4.5mm_P2.50mm" H 10650 4800 50  0001 C CNN
F 3 "~" H 10650 4800 50  0001 C CNN
	1    10650 4800
	1    0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR021
U 1 1 5F859A25
P 10450 4050
AR Path="/5F859A25" Ref="#PWR021"  Part="1" 
AR Path="/5F5F76B0/5F859A25" Ref="#PWR?"  Part="1" 
F 0 "#PWR021" H 10450 3900 50  0001 C CNN
F 1 "+3V3" H 10465 4223 50  0000 C CNN
F 2 "" H 10450 4050 50  0001 C CNN
F 3 "" H 10450 4050 50  0001 C CNN
	1    10450 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5F859A1B
P 10450 5150
AR Path="/5F859A1B" Ref="#PWR022"  Part="1" 
AR Path="/5F5F76B0/5F859A1B" Ref="#PWR?"  Part="1" 
F 0 "#PWR022" H 10450 4900 50  0001 C CNN
F 1 "GND" H 10455 4977 50  0000 C CNN
F 2 "" H 10450 5150 50  0001 C CNN
F 3 "" H 10450 5150 50  0001 C CNN
	1    10450 5150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9400 4250 9400 4550
$Comp
L Connector:Conn_01x02_Male J11
U 1 1 5F859A10
P 9200 4150
F 0 "J11" H 9173 4124 50  0000 R CNN
F 1 "Messpunkt3" H 9173 4033 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9200 4150 50  0001 C CNN
F 3 "~" H 9200 4150 50  0001 C CNN
	1    9200 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 4550 9050 4550
Text GLabel 9050 4550 0    50   Output ~ 0
A2
Wire Wire Line
	9400 4550 9400 4650
Connection ~ 9400 4550
Wire Wire Line
	9600 4550 9400 4550
Wire Wire Line
	9600 4700 9600 4550
Connection ~ 9400 5050
Wire Wire Line
	9400 5050 9400 5150
Wire Wire Line
	9400 5050 9600 5050
Wire Wire Line
	9600 4900 9600 5050
Wire Wire Line
	9400 4950 9400 5050
Wire Wire Line
	9400 4050 9400 4150
$Comp
L Device:R R3
U 1 1 5F8599FA
P 9400 4800
AR Path="/5F8599FA" Ref="R3"  Part="1" 
AR Path="/5F5F76B0/5F8599FA" Ref="R?"  Part="1" 
F 0 "R3" H 9469 4754 50  0000 L CNN
F 1 "5M" H 9469 4845 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" V 9330 4800 50  0001 C CNN
F 3 "~" H 9400 4800 50  0001 C CNN
F 4 "R" H 9400 4800 50  0001 C CNN "Spice_Primitive"
F 5 "1Meg" H 9400 4800 50  0001 C CNN "Spice_Model"
F 6 "Y" H 9400 4800 50  0001 C CNN "Spice_Netlist_Enabled"
	1    9400 4800
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5F8599ED
P 9600 4800
AR Path="/5F8599ED" Ref="C4"  Part="1" 
AR Path="/5F5F76B0/5F8599ED" Ref="C?"  Part="1" 
F 0 "C4" H 9692 4754 50  0000 L CNN
F 1 "100n" H 9692 4845 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D4.5mm_P2.50mm" H 9600 4800 50  0001 C CNN
F 3 "~" H 9600 4800 50  0001 C CNN
	1    9600 4800
	1    0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR019
U 1 1 5F8599E3
P 9400 4050
AR Path="/5F8599E3" Ref="#PWR019"  Part="1" 
AR Path="/5F5F76B0/5F8599E3" Ref="#PWR?"  Part="1" 
F 0 "#PWR019" H 9400 3900 50  0001 C CNN
F 1 "+3V3" H 9415 4223 50  0000 C CNN
F 2 "" H 9400 4050 50  0001 C CNN
F 3 "" H 9400 4050 50  0001 C CNN
	1    9400 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5F8599D9
P 9400 5150
AR Path="/5F8599D9" Ref="#PWR020"  Part="1" 
AR Path="/5F5F76B0/5F8599D9" Ref="#PWR?"  Part="1" 
F 0 "#PWR020" H 9400 4900 50  0001 C CNN
F 1 "GND" H 9405 4977 50  0000 C CNN
F 2 "" H 9400 5150 50  0001 C CNN
F 3 "" H 9400 5150 50  0001 C CNN
	1    9400 5150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8350 4250 8350 4550
$Comp
L Connector:Conn_01x02_Male J10
U 1 1 5F839A7E
P 8150 4150
F 0 "J10" H 8123 4124 50  0000 R CNN
F 1 "Messpunkt2" H 8123 4033 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 8150 4150 50  0001 C CNN
F 3 "~" H 8150 4150 50  0001 C CNN
	1    8150 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 4550 8000 4550
Text GLabel 8000 4550 0    50   Output ~ 0
A1
Wire Wire Line
	8350 4550 8350 4650
Connection ~ 8350 4550
Wire Wire Line
	8550 4550 8350 4550
Wire Wire Line
	8550 4700 8550 4550
Connection ~ 8350 5050
Wire Wire Line
	8350 5050 8350 5150
Wire Wire Line
	8350 5050 8550 5050
Wire Wire Line
	8550 4900 8550 5050
Wire Wire Line
	8350 4950 8350 5050
Wire Wire Line
	8350 4050 8350 4150
$Comp
L Device:R R2
U 1 1 5F839A68
P 8350 4800
AR Path="/5F839A68" Ref="R2"  Part="1" 
AR Path="/5F5F76B0/5F839A68" Ref="R?"  Part="1" 
F 0 "R2" H 8419 4754 50  0000 L CNN
F 1 "5M" H 8419 4845 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" V 8280 4800 50  0001 C CNN
F 3 "~" H 8350 4800 50  0001 C CNN
F 4 "R" H 8350 4800 50  0001 C CNN "Spice_Primitive"
F 5 "1Meg" H 8350 4800 50  0001 C CNN "Spice_Model"
F 6 "Y" H 8350 4800 50  0001 C CNN "Spice_Netlist_Enabled"
	1    8350 4800
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5F839A5B
P 8550 4800
AR Path="/5F839A5B" Ref="C3"  Part="1" 
AR Path="/5F5F76B0/5F839A5B" Ref="C?"  Part="1" 
F 0 "C3" H 8642 4754 50  0000 L CNN
F 1 "100n" H 8642 4845 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D4.5mm_P2.50mm" H 8550 4800 50  0001 C CNN
F 3 "~" H 8550 4800 50  0001 C CNN
	1    8550 4800
	1    0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR017
U 1 1 5F839A51
P 8350 4050
AR Path="/5F839A51" Ref="#PWR017"  Part="1" 
AR Path="/5F5F76B0/5F839A51" Ref="#PWR?"  Part="1" 
F 0 "#PWR017" H 8350 3900 50  0001 C CNN
F 1 "+3V3" H 8365 4223 50  0000 C CNN
F 2 "" H 8350 4050 50  0001 C CNN
F 3 "" H 8350 4050 50  0001 C CNN
	1    8350 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5F839A47
P 8350 5150
AR Path="/5F839A47" Ref="#PWR018"  Part="1" 
AR Path="/5F5F76B0/5F839A47" Ref="#PWR?"  Part="1" 
F 0 "#PWR018" H 8350 4900 50  0001 C CNN
F 1 "GND" H 8355 4977 50  0000 C CNN
F 2 "" H 8350 5150 50  0001 C CNN
F 3 "" H 8350 5150 50  0001 C CNN
	1    8350 5150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7300 4250 7300 4550
$Comp
L Connector:Conn_01x02_Male J9
U 1 1 5F82AD41
P 7100 4150
F 0 "J9" H 7073 4124 50  0000 R CNN
F 1 "Messpunkt1" H 7073 4033 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7100 4150 50  0001 C CNN
F 3 "~" H 7100 4150 50  0001 C CNN
	1    7100 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 4550 6950 4550
Text GLabel 6950 4550 0    50   Output ~ 0
A0
Wire Wire Line
	7300 4550 7300 4650
Connection ~ 7300 4550
Wire Wire Line
	7500 4550 7300 4550
Wire Wire Line
	7500 4700 7500 4550
Connection ~ 7300 5050
Wire Wire Line
	7300 5050 7300 5150
Wire Wire Line
	7300 5050 7500 5050
Wire Wire Line
	7500 4900 7500 5050
Wire Wire Line
	7300 4950 7300 5050
Wire Wire Line
	7300 4050 7300 4150
$Comp
L Device:R R1
U 1 1 5F7A92C2
P 7300 4800
AR Path="/5F7A92C2" Ref="R1"  Part="1" 
AR Path="/5F5F76B0/5F7A92C2" Ref="R?"  Part="1" 
F 0 "R1" H 7369 4754 50  0000 L CNN
F 1 "5M" H 7369 4845 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" V 7230 4800 50  0001 C CNN
F 3 "~" H 7300 4800 50  0001 C CNN
F 4 "R" H 7300 4800 50  0001 C CNN "Spice_Primitive"
F 5 "1Meg" H 7300 4800 50  0001 C CNN "Spice_Model"
F 6 "Y" H 7300 4800 50  0001 C CNN "Spice_Netlist_Enabled"
	1    7300 4800
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5F7A929E
P 7500 4800
AR Path="/5F7A929E" Ref="C1"  Part="1" 
AR Path="/5F5F76B0/5F7A929E" Ref="C?"  Part="1" 
F 0 "C1" H 7592 4754 50  0000 L CNN
F 1 "100n" H 7592 4845 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D4.5mm_P2.50mm" H 7500 4800 50  0001 C CNN
F 3 "~" H 7500 4800 50  0001 C CNN
	1    7500 4800
	1    0    0    1   
$EndComp
$Comp
L power:+3V3 #PWR013
U 1 1 5F7A9298
P 7300 4050
AR Path="/5F7A9298" Ref="#PWR013"  Part="1" 
AR Path="/5F5F76B0/5F7A9298" Ref="#PWR?"  Part="1" 
F 0 "#PWR013" H 7300 3900 50  0001 C CNN
F 1 "+3V3" H 7315 4223 50  0000 C CNN
F 2 "" H 7300 4050 50  0001 C CNN
F 3 "" H 7300 4050 50  0001 C CNN
	1    7300 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5F7A9292
P 7300 5150
AR Path="/5F7A9292" Ref="#PWR014"  Part="1" 
AR Path="/5F5F76B0/5F7A9292" Ref="#PWR?"  Part="1" 
F 0 "#PWR014" H 7300 4900 50  0001 C CNN
F 1 "GND" H 7305 4977 50  0000 C CNN
F 2 "" H 7300 5150 50  0001 C CNN
F 3 "" H 7300 5150 50  0001 C CNN
	1    7300 5150
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5F750F6B
P 5900 6900
AR Path="/5F750F6B" Ref="C6"  Part="1" 
AR Path="/5F5F76B0/5F750F6B" Ref="C?"  Part="1" 
F 0 "C6" H 5850 7400 50  0000 L CNN
F 1 "100n" V 5900 7450 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_Tantal_D4.5mm_P2.50mm" H 5900 6900 50  0001 C CNN
F 3 "~" H 5900 6900 50  0001 C CNN
	1    5900 6900
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x15 J15
U 1 1 60057748
P 3300 5050
F 0 "J15" V 3517 5046 50  0000 C CNN
F 1 "Conn_01x15" V 3426 5046 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x15_P2.54mm_Vertical" H 3300 5050 50  0001 C CNN
F 3 "~" H 3300 5050 50  0001 C CNN
	1    3300 5050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2700 5250 2700 5600
Wire Wire Line
	2800 5250 2800 5700
Wire Wire Line
	2900 5250 2900 5800
Wire Wire Line
	3000 5250 3000 5950
Wire Wire Line
	3100 5250 3100 6050
Wire Wire Line
	3500 5250 3500 6500
Wire Wire Line
	3600 5250 3600 6600
Wire Wire Line
	3300 5250 3300 6750
Wire Wire Line
	3400 5250 3400 6850
Wire Wire Line
	3800 5250 3800 7000
Wire Wire Line
	3900 5250 3900 7200
Wire Wire Line
	3700 5250 3700 7100
NoConn ~ 4000 5250
Connection ~ 3900 7200
Wire Wire Line
	2400 7200 3900 7200
$Comp
L Connector:Conn_01x05_Female J1
U 1 1 6032F579
P 1000 1350
F 0 "J1" H 892 925 50  0000 C CNN
F 1 "Conn_01x05_Female" H 892 1016 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H 1000 1350 50  0001 C CNN
F 3 "~" H 1000 1350 50  0001 C CNN
	1    1000 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	1200 1150 1350 1150
Wire Wire Line
	1600 1400 1350 1400
Wire Wire Line
	1350 1400 1350 1550
Wire Wire Line
	1350 1550 1200 1550
NoConn ~ 1200 1250
NoConn ~ 1200 1350
NoConn ~ 1200 1450
Text Notes 4400 1700 0    50   ~ 0
RST
Text Notes 3400 1700 2    50   ~ 0
RST
NoConn ~ 4400 1700
Wire Wire Line
	2600 5250 2600 6300
Wire Wire Line
	3200 5250 3200 6150
Connection ~ 2700 5600
Wire Wire Line
	2700 5600 4150 5600
Connection ~ 2800 5700
Wire Wire Line
	2800 5700 4150 5700
Connection ~ 2900 5800
Wire Wire Line
	2900 5800 4150 5800
Connection ~ 3000 5950
Wire Wire Line
	3000 5950 5400 5950
Connection ~ 3100 6050
Wire Wire Line
	3100 6050 5300 6050
Connection ~ 3200 6150
Wire Wire Line
	3200 6150 5200 6150
Wire Wire Line
	2400 5600 2700 5600
Wire Wire Line
	2400 5700 2800 5700
Wire Wire Line
	2400 5800 2900 5800
Wire Wire Line
	2400 5950 3000 5950
Wire Wire Line
	2400 6050 3100 6050
Wire Wire Line
	2400 6150 3200 6150
Connection ~ 2600 6300
Wire Wire Line
	2600 6300 5600 6300
Wire Wire Line
	2400 6300 2600 6300
Connection ~ 3800 7000
Wire Wire Line
	3800 7000 4100 7000
Wire Wire Line
	2400 7000 3800 7000
Wire Wire Line
	2400 7100 3700 7100
Connection ~ 3700 7100
Wire Wire Line
	3700 7100 4000 7100
Connection ~ 3500 6500
Wire Wire Line
	3500 6500 4150 6500
Connection ~ 3600 6600
Wire Wire Line
	3600 6600 4150 6600
Wire Wire Line
	2400 6500 3500 6500
Wire Wire Line
	2400 6600 3600 6600
Connection ~ 3300 6750
Wire Wire Line
	3300 6750 4900 6750
Connection ~ 3400 6850
Wire Wire Line
	3400 6850 4900 6850
Wire Wire Line
	2400 6750 3300 6750
Wire Wire Line
	2400 6850 3400 6850
$EndSCHEMATC
